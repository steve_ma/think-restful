<?php
namespace stevema\restful\traits;
use app\validate\AfsValidate;
use stevema\restful\RestfulException;

trait Save
{
    public function save(){
        # 获取验证模型
        $validate = $this->getValidate();
        # 注册验证表单->开始认证
        if(!empty($validate)) {
            validate($validate)
                ->batch(true)
                ->scene('save')
                ->check(request()->all());
        }
        # 获取参数- -
        $payload=request()->all();
        # 插入数据
        $currentModel = $this->performSave($payload);
        # 获取资源解释器
        $resource  = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        return json($currentModel->toArray(), 201);
    }
    public function performSave($payload){
        $model = $this->getModel();
        $currentModel = $model::create($payload);
        return $currentModel;
    }
}
