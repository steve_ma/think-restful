<?php
namespace stevema\restful\traits;
use stevema\restful\RestfulException;

trait Update
{
    public function update(){
        # 获取验证模型
        $validate = $this->getValidate();
        # 注册验证表单->开始认证
        if(!empty($validate)) {
            validate($validate)
                ->batch(true)
                ->scene('save')
                ->check(request()->all());
        }
        # 获取参数- -
        $payload = $this->getFilteredPayload();
        # 获取模型数据
        $currentModel = $this->getCurrentModel('perShowQuery');
        # 修改数据
        $currentModel = $this->performUpdate($currentModel, $payload);
        # 获取资源解释器
        $resource  = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        return json($currentModel->toArray());
    }

    public function performUpdate($currentModel, $payload){
        $currentModel->save($payload);
        return $currentModel;
    }

    public function patch(){
        # 获取验证模型
        $validate = $this->getValidate();
        # 注册验证表单->开始认证
        if(!empty($validate)) {
            validate($validate)
                ->batch(true)
                ->scene('patch')
                ->check(request()->all());
        }
        # 获取参数- -
        $payload = $this->getFilteredPayload();
        # 获取模型数据
        $currentModel = $this->getCurrentModel('perShowQuery');
        # 修改数据
        $currentModel = $this->performUpdate($currentModel, $payload);
        # 获取资源解释器
        $resource  = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        return json($currentModel->toArray());
    }
}
