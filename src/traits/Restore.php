<?php
namespace stevema\restful\traits;
use stevema\restful\RestfulException;
trait Restore
{
    /**
     * @throws RestfulException
     */
    public function restore(){
        # 获取模型数据
        $currentModel = $this->getCurrentModel('withTrashed');
        $this->performRestore($currentModel);
        return json([], 204);
    }
    public function performRestore($currentModel){
        $result = $currentModel->restore();
        return $result;
    }
}
