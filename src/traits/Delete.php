<?php
namespace stevema\restful\traits;
use stevema\restful\RestfulException;
trait Delete
{
    /**
     * @throws RestfulException
     */
    public function delete(){
        # 获取模型数据
        $currentModel = $this->getCurrentModel();
        $this->performDelete($currentModel);
        return json([], 204);
    }
    public function performDelete($currentModel){
        $result = $currentModel->delete();
        return $result;
    }
}
