<?php
namespace stevema\restful\traits;
use stevema\restful\RestfulException;
use stevema\restful\RestfulFilter;

trait Index
{
    /**
     * @throws RestfulException
     */
    public function index(){
        $filter   = $this->getFilter();
        $model = $this->getModel();
        $query = $model::query();
        $query = $this->perIndexQuery($query);
        if ($filter) {
            $filter = new $filter($query);
        } else {
            $filter = new RestfulFilter($query);
        }
        $resource = $this->getResource();
        $result = $filter->getData($resource);
        return json($result);
    }

    public function perIndexQuery($query){
        # 如果有想提前执行的过滤 这里可以使用
        # 比如 前端的接口 看article的时候必须发布的才能看得到
        # 还不能是预先设置发布时间的
//        $query->where("is_published", '=', 1);
//        $query->where("published_at", '<=', time());
        $query->field("*");
        return $query;
    }
}
