<?php
namespace stevema\restful\traits;
use stevema\restful\RestfulException;
trait Read
{
    public function read(){
        # 获取模型数据
        $currentModel = $this->getCurrentModel('perReadQuery');
        # 获取资源解释器
        $resource = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        return json($currentModel->toRead());
    }
    public function perReadQuery($query){
        # 如果有想提前执行的过滤 这里可以使用
        # 比如 前端的接口 看article的时候必须发布的才能看得到
        # 还不能是预先设置发布时间的
//        $query->where("is_published", '=', 1);
//        $query->where("published_at", '<=', time());
        $query->field("*");
        return $query;
    }
}
