<?php
namespace stevema\restful\traits;
use stevema\restful\RestfulException;
// 多态关联的时候 model use这个可以自定义 type 的 别名
trait UseTableNameAsMorphClass
{
    public function getMorphClass()
    {
        return $this->getTable();
    }
}
