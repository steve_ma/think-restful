<?php
namespace stevema\restful;

class RestfulResource {
    protected $instance = null;
    public function __construct($instance){
        $this->instance = $instance;
    }

    public function __get($name){
        return $this->instance->$name;
    }

    public function __call($name, $options){
        return $this->instance->$name(...$options);
    }
    public function __toString(){
        return $this->instance;
    }

    public function toIndex(){
        //你可以使用模型的hidden/visible/append/withAttr方法进行数据集的输出处理
        return $this->instance->toArray();
    }

    public function toRead(){
        //你可以使用模型的hidden/visible/append/withAttr方法进行数据集的输出处理
        return $this->instance->toArray();
    }
}