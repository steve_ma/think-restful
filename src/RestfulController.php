<?php
namespace stevema\restful;

use think\Model;
use stevema\restful\RestfulFilter;
use stevema\restful\RestfulException;
use app\model\api\v2\ConfigModel;
class RestfulController {
    /**
     * 过滤插件  列表传过来的参数需要一些方法来走到模型里面
     * 请继承 Stevema\Restful\Filters\RestfulFilter
     * 可以没有 但是不能乱搞
     */
    protected const FILTER = RestfulFilter::class;
    /**
     * 模型 对应的表
     * 应当继承 think\Model
     */
    protected const MODEL = null;

    /**
     * 模型解释器 -> 模型查出来的数据 有些不想放出去的 就可以使用这个来处理
     * 应当继承 Illuminate\Http\Resources\Json\JsonResource
     */
    protected const RESOURCE = null;

    /**
     * request ->  post  put patch 使用到的表单验证
     * 应当继承 think\Validate
     * 继承其他的验证也可以
     * 一定一定要写scene方法
     */
    protected const VALIDATE = null;

    /**
     * 路由绑定的参数名称
     * ROUTE_KEY 模型在路由中应当保持一致 然后就是应当是Model的小写
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * ROUTE_KEYMAP 关系模型时用到的
     * 比如 Route::get('/smorders/{smorder}/smskus/{smsku}', function(){})
     * SmOrder 和 SmSku 可以是一对一 也可以是一对多
     * 这样就需要 ROUTE_KEY来确认接收哪个参数了
     * ROUTE_KEYMAP 来对应相关的值 -> 在smsku 中 如果smorder关联的是 order_id
     * ROUTE_KEYMAP = ['smorder' => 'order_id]  这样来与smorder对应
     *
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * 只有一个模型的时候不需要 ROUTE_KEY 和 ROUTE_KEYMAP
     *
     * @var string
     */
    protected const ROUTE_KEY = null;
    protected const ROUTE_KEYMAP = [];
    /**
     * 获取路由id
     */
    protected function getRouteKey(){
        $key = static::ROUTE_KEY;
        if(empty($key)) $key = $this->getModelRouteName();
        return $key;
    }
    protected function getCurrentId()
    {
        $key = $this->getRouteKey();
        $value = request()->route($key);
        if(empty($value)){
            $params = $this->getRouteValues();
            return current($params);
        }
        return $value;
    }
    protected function getRouteValues(){
        return request()->route();
    }
    protected function queryRouteValues($query){
        $key = $this->getRouteKey();
        $params = $this->getRouteValues();
        if(count($params) > 1) {
            foreach ($params as $k => $v) {
                if ($k != $key && isset(static::ROUTE_KEYMAP[$k])) {
                    $query->where(static::ROUTE_KEYMAP[$k], '=', $v);
                }
            }
        }
        return $query;
    }
    /**
     * 获取模型
     */
    protected function getModel()
    {
        return static::MODEL;
    }

    /**
     * 获取模型的名字后小写
     * @return string
     */
    protected function getModelRouteName()
    {
        $arr = explode('\\', static::MODEL);
        return str_replace("model", '', strtolower(end($arr)));
    }

    /**
     * 获取解释器
     */
    protected function getResource()
    {
        return static::RESOURCE;
    }

    /**
     * 修改数据的时候用到的-获取要修改的参数
     * @return array
     */
    protected function getFilteredPayload(): array
    {
        $model = $this->getModel();
        $fillable = array_keys($model::getFields());
        return $fillable === [] ? request()->all() : request()->only($fillable);
    }

    /**
     * 获取过滤器
     * @return string
     */
    protected function getFilter()
    {
        return static::FILTER;
    }
    protected function getValidate()
    {
        return static::VALIDATE;
    }

    /**
     * 获取模型数据
     * @param null $method
     * @param int $is_trashed
     * @return mixed
     */
    protected function getCurrentModel($method=null){
        $currentId    = $this->getCurrentId();
        $model = $this->getModel();
        $query = $model::query();
        if($method && method_exists($this, $method)){
            $query = $this->{$method}($query);
        }
        # 遍历路由里面的参数 并且query一下
        $query = $this->queryRouteValues($query);
        $currentModel = $query->findorfail($currentId);
        return $currentModel;
    }
}