<?php
namespace stevema\restful;

use think\Service as BaseService;

class RestfulService extends BaseService
{
    public function boot()
    {

        $this->commands([
            \stevema\restful\commands\Create::class,
        ]);
    }
}