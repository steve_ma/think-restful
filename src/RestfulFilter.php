<?php

namespace stevema\restful;

use stevema\restful\RestfulException;
use think\db\Query;
class RestfulFilter {
    /**
     * 分页用到的页码参数key
     */
    protected const KEY_PAGE = 'page';
    /**
     * 分页用到的每页显示条数参数key
     */
    protected const KEY_SIZE = 'size';
    /**
     * 分页用到的 cursor 参数key
     */
    protected const KEY_CURSOR = 'cursor';
    /**
     * 默认页数
     */
    protected const DEFAULT_PAGE = 1;
    /**
     * 默认条数
     */
    protected const DEFAULT_SIZE = 15;
    /**
     * 排序用到的参数key
     *  paginateX 分页的时候  ordering一定要是主键- 不然会出现不可预知的错误
     */
    protected const KEY_ORDERING = 'ordering';
    /**
     * 默认排序规则 多个中间加逗号  比如 "-type,-id"
     */
    protected const DEFAULT_ORDERING = '-id';
    /**
     * 允许排序的字段 空则不限制 所有的参数都可以参与排序
     */
    protected const ACCEPT_ORDERINGS = ["id", 'create_at'];
    /**
     * 允许的检索字段 keys  空则不限制 所有的参数都可以参与过滤
     * 注意这里只有id 没有前面的-号
     */
    protected const ACCEPT_FILTER_KEYS = [];
    /**
     * 查询的参数列表
     * 不建议设置 和resource 一起用的时候可能这边没有的值那边会出问题
     */
    protected const CLUMNS = ['*'];
    /**
     * 分页方法 noPaginate 无分页 、 paginate 默认分页 、 simplePaginate 简单分页 、 cursorPaginate cursor分页
     */
    protected const PAGINATOR = 'paginate';
    /**
     * 默认分页方法
     */
    protected const DEFAULT_PAGINATOR = 'noPaginate';
    /**
     * 允许的分页方法列表
     */
    protected const ACCEPT_PAGINATORS = [
        'noPaginate','paginate','simplePaginate','paginateX'
    ];

    /**
     * 资源解释器 - 返回之前重新编辑一下输出的数组
     */
    protected const FILTER_RESOURCE = null;

    /**
     * @var Query
     */
    private Query $query;

    /**
     * @param Query $query
     * @throws RestfulException
     */
    public function __construct(Query $query)
    {
        $this->query = $query;
        $this->queryOrdering();
        $this->queryFilters();
    }

    /**
     * 获取分页名称
     * @return string
     */
    protected function getPaginatorName(): string
    {
        if(!in_array(static::PAGINATOR, static::ACCEPT_PAGINATORS)){
            return static::DEFAULT_PAGINATOR;
        }
        return static::PAGINATOR;
    }

    /**
     * 是不是 cursor 分页
     * @return bool
     */
    protected function isCursor(): bool
    {
        return static::PAGINATOR == 'paginateX';
    }

    /**
     * 有没有启用分页
     * @return bool
     */
    protected function isPaginate(): bool
    {
        return $this->getPaginatorName() != static::DEFAULT_PAGINATOR;
    }

    /**
     * 不可用的参数们 这些参数不会用于检索和排序
     * @return array
     */
    private function getUnuseParams(): array
    {
        $arr = [
            static::KEY_ORDERING,
        ];
        if($this->isPaginate()) {
            $arr[] = static::KEY_SIZE;
            if ($this->isCursor()) {
                $arr[] = static::KEY_CURSOR;
            } else {
                $arr[] = static::KEY_PAGE;
            }
        }
        return $arr;
    }

    /**
     * 基础过滤方法 list_filters里面的参数
     * 字段_filter 方法可以自己写过滤
     * 比如
     * public function name_filter($key, $val){
     *      $query = $this->getQuery();
     *      $query->where("sex", '=', 2);
     * }
     * @return void
     */
    private function queryFilters(): void
    {
        if(empty($this->queryparams) === FALSE) {
            $unused = $this->getUnuseParams();
            foreach ($this->queryparams as $key => $value) {
                if (!in_array($key, $unused)) {
                    if(empty(static::ACCEPT_FILTER_KEYS) || in_array($key, static::ACCEPT_FILTER_KEYS)) {
                        $method = $key . "_filter";
                        if (method_exists($this, $method)) {
                            $this->{$method}($key, $value);
                        } else {
                            if (is_array($value)) {
                                $this->query->whereIn($key, $value);
                            } else {
                                $this->query->where($key, '=', $value);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 基础排序方法 根据参数获取排序规则
     * @return void
     */
    private function queryOrdering(): void
    {
        $ordering_params = $this->getOrderingParams();
        foreach ($ordering_params as $order => $sort) {
            if (empty(static::ACCEPT_ORDERINGS) || in_array($order, static::ACCEPT_ORDERINGS)) {
                # paginateX 的时候 ording一定是主键
                if(! $this->isCursor()) {
                    $this->query->order($order, $sort);
                } else {
                    $this->_order = $order;
                    $this->_sort = $sort;
                }
            }
        }

    }

    /**
     * 获取排序的参数
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getOrderingParams(): array
    {
        $orderings = request()->get(static::KEY_ORDERING, static::DEFAULT_ORDERING);
        $ordering_arr = explode(",", $orderings);
        unset($orderings);
        $ordering_params = [];
        foreach($ordering_arr as $ordering){
            $order = $ordering;
            $sort = 'asc';
            if(str_starts_with($ordering, '-')){
                $order = substr($ordering, 1);
                $sort = 'desc';
            }
            $ordering_params[$order] = $sort;
            unset($order);
            unset($sort);
        }
        return $ordering_params;
    }


    /**
     * 返回 query
     * @return Query|null
     */
    public function getQuery(): ?Query
    {
        return $this->query;
    }

    /**
     * 获取当前的页码
     * @return int
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getSize(): int
    {
        $size = request()->get(static::KEY_SIZE, static::DEFAULT_SIZE);
        return intval($size);
    }

    /**
     * 获取分页用到的页码key或者Cursor的key
     * @return string
     */
    private function getPageKeyOrCursorKey(): string
    {
        if($this->isCursor()){
            return static::KEY_CURSOR;
        }
        return static::KEY_PAGE;
    }

    /**
     * 没有分页的返回情况
     * @param array $arr
     * @param $resource
     * @return mixed
     */
    public function noPaginatorData(Array $arr, $resource): mixed
    {
        return $resource::collection($arr['data']);
    }

    /**
     * 获取Model数据-可能是分页之后的
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getQueryData()
    {
        $this->queryFilters();
        $query = $this->getQuery();
        if($this->isPaginate()) {
            # 分页查询
//            'noPaginate','paginate','simplePaginate','paginateX'
            if($this->getPaginatorName() == 'paginate'){
                # paginate
                $paginate = $query->field(static::CLUMNS)->{$this->getPaginatorName()}([
                    'list_rows'=> $this->getSize(),
                    'var_page' => $this->getPageKeyOrCursorKey(),
                ]);
            } elseif($this->getPaginatorName() == 'simplePaginate'){
                # simplePaginate
                $paginate = $query->field(static::CLUMNS)->paginate(
                    $this->getSize(),true
                );
            } else {
                # paginateX
                echo 1;
                $paginate = $query->field(static::CLUMNS)->{$this->getPaginatorName()}(
                    $this->getSize(),$this->_order,$this->_sort
                );
            }
        } else {
            # 不分页  全部查出来
            $paginate = $query->field(static::CLUMNS)->select();
        }
        return $paginate;
    }

    public function getData($resource=null){
        $paginate = $this->getQueryData();
        if(!empty($resource)) {
            $paginate_data = new $resource($paginate);
        } else {
            $paginate_data = $paginate;
        }
        return $this->paginateDate($paginate_data->toIndex());
    }

    public function paginateDate($array){
        if(!empty(static::FILTER_RESOURCE)){
            return static::FILTER_RESOURCE::{$this->getPaginatorName()}($array, $this->getPageKeyOrCursorKey());
        }
        return $array;
    }
}
