<?php
namespace stevema\restful\commands;

use InvalidArgumentException;
use RuntimeException;
use Phinx\Util\Util;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Facade\Console;
class Create extends Command
{
    protected string $full = '';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('make:restful')
            ->setDescription('Create a new restful')
            ->addArgument('name', Argument::REQUIRED, 'What is the name of the restful?')
            ->addOption('full','f', Option::VALUE_NONE, 'make restful with full')
            ->setHelp(sprintf('%sCreates a new restful %s', PHP_EOL, PHP_EOL));
    }

    /**
     * Create the new migration.
     *
     * @param Input  $input
     * @param Output $output
     * @return void
     */
    protected function execute(Input $input, Output $output)
    {
        $name = $input->getArgument('name');
        $options = $input->getOptions();
        $namePath = '';
        $className = $name;
        $this->full = $options['full']?'.full':'';

        $nameArr = explode('/', $name);
        if(count($nameArr) > 1){
            $className = end($nameArr);
            $namePath .= str_replace(['/'.$className], [''], $name);
            $className = ucfirst($className);
        }

        $model = $this->makeModel($namePath, $className);
        $validate = $this->makeValidate($namePath, $className);
        $resource = $this->makeResource($namePath, $className);
        $filter = $this->makeFilter($namePath, $className);
        $this->makeController($namePath, $className, $options=[
            'validate' => $validate,
            'resource' => $resource,
            'filter' => $filter,
            'model' =>  $model,
        ]);
    }

    public function makeModel($namePath, $className){
        try{
            $className = ucfirst(str_replace(["/","\\"], ["",""], $className))."Model";
            $nameSpace = str_replace("/", "\\", $namePath);
            $output = Console::call('make:model',[
                $namePath."/".$className
            ]);
            $classStr = "app\\model\\".$nameSpace."\\".$className."::class";
            $this->output->writeln($output->fetch());
            return $classStr;
        } catch (\Exception $e){
            $this->output->writeln('<error>Model:</error> .' .$e->getMessage());
            return $classStr;
        }

    }
    public function makeController($namePath, $className, $options=[]){
        return $this->makeFileByName($namePath, $className, 'controller',$options);
    }
    public function makeValidate($namePath, $className){
        return $this->makeFileByName($namePath, $className, 'validate',[]);
    }

    public function makeFileByName($namePath, $className, $type, $options=[]){
        try{
            $className .= ucfirst($type);
            $nameSpace = "app\\{$type}\\".str_replace("/", "\\", $namePath);
            $classStr = $nameSpace."\\".$className."::class";
            $dirPath = base_path("{$type}/".$namePath);
            $stub_path = realpath(__DIR__ . "/stubs/{$type}{$this->full}.stub");
            $this->checkDirectory($dirPath);
            $filePath = $dirPath . $className . '.php';
            $this->checkClassFile($className, $filePath);
            $replace_first = [
                '{classname}', '{namespace}',
            ];
            $replace_second = [
                $className, $nameSpace,
            ];
            if(!empty($options)){
                foreach($options as $k => $v){
                    $replace_first[] = "{".$k."}";
                    $replace_second[] = "\\".$v;
                }
            }

            $contents = str_replace($replace_first,$replace_second,file_get_contents($stub_path));
            if (false === file_put_contents($filePath, $contents)) {
                throw new RuntimeException(sprintf('The file "%s" could not be written to', $filePath));
            }
            $this->output->writeln("<info>".ucfirst($type).": " . str_replace(getcwd(), '', $filePath).'</info>');
            return $classStr;
        } catch (\Exception $e){
            $this->output->writeln("<error>".ucfirst($type).": " .$e->getMessage().'</error>');
            return $classStr;
        }
    }
    public function makeFilter($namePath, $className){
        return $this->makeFileByName($namePath, $className, 'filter',[]);
    }
    public function makeResource($namePath, $className){
        return $this->makeFileByName($namePath, $className, 'resource',[]);
    }

    public function checkDirectory($dirPath){
        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0755, true);
        }

        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException(sprintf('directory "%s" does not exist', $dirPath));
        }

        if (!is_writable($dirPath)) {
            throw new InvalidArgumentException(sprintf('directory "%s" is not writable', $dirPath));
        }
    }
    public function checkClassFile($className, $filePath){
        if (!Util::isValidPhinxClassName($className)) {
            throw new InvalidArgumentException(sprintf('class name "%s" is invalid. Please use CamelCase format.', $className));
        }
        if (is_file($filePath)) {
            throw new InvalidArgumentException(sprintf('The file "%s" already exists', $filePath));
        }
    }
}
