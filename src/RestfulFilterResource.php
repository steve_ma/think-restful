<?php

namespace stevema\restful;

class RestfulFilterResource {
    public static function noPaginate($array, $cursor_key){
        return $array;
    }
    public static function paginate($array, $cursor_key){
        return [
            'total' => $array['total'],
            'size' => $array['per_page'],
            'current_page' => $array['current_page'],
            'last_page' => $array['last_page'],
            'data' => $array['data'],
        ];
    }
    public static function simplePaginate($array, $cursor_key){
        return [
            'size' => $array['per_page'],
            'current_page' => $array['current_page'],
            'data' => $array['data'],
        ];
    }
    public static function cursorPaginate($array, $cursor_key){
        return [
            'size' => $array['per_page'],
            'current_page' => $array['current_page'],
            'data' => $array['data'],
        ];
    }
}
